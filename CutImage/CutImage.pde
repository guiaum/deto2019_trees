/**
 * Pixel Array. 
 * 
 * Click and drag the mouse up and down to control the signal and 
 * press and hold any key to see the current pixel being read. 
 * This program sequentially reads the color of every pixel of an image
 * and displays this color to fill the window.  
 */

PImage a;
int direction = 1;
float signal;

int[][] aPixels;
int[][] values;

int maxY, maxX = 0;
int minY = 720;
int minX = 1280;
int imageWidth;
int imageHeight;

PGraphics exportedImage;

void setup() {
  size(1280, 720);
  frameRate(30);
  smooth();
  println(minY);

  // Load the image into a new array
  // Extract the values and store in an array
  a = loadImage("test1.png");
  aPixels = new int[a.width][a.height];
  values = new int[a.width][a.height];
  noFill();
  a.loadPixels();
  for (int i = 0; i < a.height; i++) {
    for (int j = 0; j < a.width; j++) {
      aPixels[j][i] = a.pixels[i*a.width + j];
      values[j][i] = int(alpha(aPixels[j][i]));
      if (values[j][i]>0 && i>maxY)maxY = i;
      if (values[j][i]>0 && i<minY)minY = i;
      if (values[j][i]>0 && j>maxX)maxX = j;
      if (values[j][i]>0 && j<minX)minX = j;
    }
  }

  imageWidth = maxX-minX;
  imageHeight =  maxY-minY;
  println(minY, maxY, minX, maxX, imageWidth, imageHeight);
  
  PImage cuttedImage = a.get(minX,minY,imageWidth,imageHeight);
  
  
  /*
  cuttedImage = createGraphics(imageWidth, imageHeight);
  cuttedImage.beginDraw();
  cuttedImage.image(a, 0 , 0);
  cuttedImage.endDraw();
  */

  exportedImage = createGraphics(600, 600);
 
  exportedImage.beginDraw();
  if (imageWidth < imageHeight)
  {
    float ratio = 600.0/float(imageHeight);
    float newWidth = imageWidth*ratio;
    exportedImage.image(cuttedImage, (600-newWidth)/2, 0 , newWidth, 600);
  } else
  {
    float ratio = 600.0/float(imageWidth);
    float newHeight = imageHeight*ratio;
    exportedImage.image(cuttedImage, 0, (600-newHeight)/2, 600, newHeight);
  }
  exportedImage.endDraw();
    //temp.image(a,0,0);
  exportedImage.save("new.png");
  
}

void draw() {
  background(255, 0, 0);
  image(a, 0, 0);  // fast way to draw an image
  stroke(255, 255, 0);
  rect(minX, minY, imageWidth, imageHeight);
}
