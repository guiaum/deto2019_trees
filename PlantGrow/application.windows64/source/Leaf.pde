// Coding Rainbow
// Daniel Shiffman
// http://patreon.com/codingtrain
// Code for: https://youtu.be/JcopTKXt8L8


class Leaf {
  PVector pos;
  boolean reached = false;

  Leaf() {
    
    pos = PVector.random3D();
    pos.mult(random(width/int(random(1,4))));
    pos.y -= height/int(random(4,8));
    
 
  }

  void reached() {
    reached = true;
  }

  void show() {
    //if (reached)
    //{
    pg.fill(255,0,0);
    pg.noStroke();
    pg.pushMatrix();
    pg.translate(pos.x, pos.y, pos.z);
    //sphere(4);
    pg.ellipse(0,0, 10, 10);
    pg.popMatrix();
  //}
  }
}
