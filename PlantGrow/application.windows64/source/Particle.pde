class Particle {
 
  float x;
  float y;
  float alpha = 255;
  
  float velX; // speed or velocity
  float velY;
  
  
  Particle () {
   //x and y position to be in middle of screen
    x = width/2;
    y = height/4*3;
    
    
    velX = random (-20,20);
    velY = random (-25,25);
    
   
  }
  
  void update () {
    
    x+=velX;
    y+=velY;
    
    pg.fill (255, alpha);
    pg.noStroke();
    pg.ellipse (x,y,3,3);
    alpha -=5;
  }
  
  float getAlpha()
  {
   return alpha; 
  }
  
  
}
