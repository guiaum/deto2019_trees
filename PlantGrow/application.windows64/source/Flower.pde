// Coding Rainbow
// Daniel Shiffman
// http://patreon.com/codingtrain
// Code for: https://youtu.be/JcopTKXt8L8



class Flower {
  // Position on the branch, when created
  PVector pos;
  
  /**
  *Flower petal
  *
  *                   sk2
  *                    |
  *                    |
  *                    |
  *                    |
  *                    |
  *                    |
  *                    sk1--------n1
  *                    |
  *                    |
  *                    |
  *                    |
  *                  origin(=pos)
  */
  PVector sk1;
  PVector sk2;
  PVector p1;
  PVector n1;
  PVector n2;
  
  //ArrayList to store petals
  ArrayList <PVector[]>  petals = new ArrayList <PVector[]>();
  int nbrOfPetals =  int (random(10, 50));
  
  // Stored color of each point
  color[] c = new color[5];
    int chosenColor1;
  int chosenColor2;

  
  // Scale of leaf
  float theScale;
  float tempScale = 0;
    
  //Alpha
  float alpha = 255;
  
  Flower(PVector _pos, int _chosenColor1, int _chosenColor2) {
    pos = _pos;
    chosenColor1 = _chosenColor1;
    chosenColor2 = _chosenColor2;
    
    //Var width of petal
    PVector offset = PVector.mult(PVector.random3D(), random(10));
    
    for (int i=0; i<nbrOfPetals; i++)
    {
     PVector[] myArrayOfPoints = new PVector[3];
     //Point1
     PVector storedSk1 =  PVector.random3D();
     storedSk1.mult(random(10));
     myArrayOfPoints[0] = storedSk1;
     
     //Point2
     PVector storedSk2 = new PVector();
     PVector.mult(storedSk1,2,storedSk2);
     myArrayOfPoints[1] = storedSk2;
     
     //Point3
     PVector storedN1 = new PVector();
     PVector.add(storedSk1,offset,storedN1);
     myArrayOfPoints[2] = storedN1;     
     
     petals.add(myArrayOfPoints);
    }

        // COLORS
    c[0] = color(255, 255, 255); // set the top color (a bit lighter)
    c[1] = color(255, 160, 222); // set the top color (a bit lighter)
    c[2] = color(62, 33, 255); // set the top color (a bit lighter)
    c[3] = color(160, 27, 23); // set the top color (a bit lighter)
    c[4] = color(252, 252, 64); // set the top color (a bit lighter)
   
     float tirage = random(100);
    if (tirage<25)
    {
      chosenColor1 = int(random(c.length));
    } else if (tirage>=25 && tirage <40)
    {
      chosenColor2 = int(random(c.length));
    } 

    
   //Echelle de la feuille
   theScale = random(2,5);
 
        
  }

  void show() {
    pg.pushMatrix();
    pg.translate(pos.x, pos.y, pos.z);
    pg.scale(tempScale);
    
    pg.noFill();
    pg.strokeWeight(1);
    for (int i=0; i<nbrOfPetals; i++)
    {
    PVector[] myArrayOfPoints = petals.get(i);
    pg.beginShape(TRIANGLE);
    
    pg.stroke(c[0], alpha);
    pg.vertex(0, 0, 0);
    pg.stroke(c[chosenColor1]);
    pg.vertex(myArrayOfPoints[1].x, myArrayOfPoints[1].y, myArrayOfPoints[1].z);
    pg.stroke(c[chosenColor2]);
    pg.vertex(myArrayOfPoints[2].x, myArrayOfPoints[2].y, myArrayOfPoints[2].z);
    pg.endShape();
    }
    /*DEBUG
    line(sk1.x, sk1.y, sk1.z,n1.x, n1.y, n1.z);
    line(sk1.x, sk1.y, sk1.z,n2.x, n2.y, n2.z);
    */
    pg.popMatrix();
  
  }
  
  void grow()
  {
    if (tempScale < theScale)
    {
      tempScale+=0.02;
    }
  }
}
