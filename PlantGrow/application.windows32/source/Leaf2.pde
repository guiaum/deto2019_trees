// Coding Rainbow
// Daniel Shiffman
// http://patreon.com/codingtrain
// Code for: https://youtu.be/JcopTKXt8L8



class Leaf2 {
  // Position on the branch, when created
  PVector pos;

  /**
   *3D Leaf
   *
   *                   sk2
   *                    |
   *                    |
   *                    |
   *                    |
   *                    |
   *                    |
   *          n2-------sk1--------n1
   *                    |
   *                    |
   *                    |
   *                    |
   *                    p1  
   *                    |
   *                    |
   *                    |
   *                  origin(=pos)
   */
  PVector sk1;
  PVector sk2;
  PVector p1;
  PVector n1;
  PVector n2;

  // Stored color of each point
  color[] c = new color[5];
  int chosenColor1;
  int chosenColor2;

  // Scale of leaf
  float theScale;
  float tempScale = 0;

  //Alpha
  float alpha = 255;

  //Size of leaves
  float sizeOfLeaves;
  float widthOfLeaves;

  //Rotate the leaf ?
  float rotateTheLeaf = random(PI);

  Leaf2(PVector _pos, int _chosenColor1, int _chosenColor2, float _sizeOfLeaves, float _widthOfLeaves) {
    pos = _pos;
    chosenColor1 = _chosenColor1;
    chosenColor2 = _chosenColor2;
    sizeOfLeaves = _sizeOfLeaves;
    widthOfLeaves = _widthOfLeaves;
    //Points
    sk1 =  PVector.random3D();
    //VAR (50) -> size of leaf
    sk1.mult(random(50));
    sk2 = new PVector();
    PVector.mult(sk1, 2, sk2);
    //VAR (30) -> Courbure de la feuille
    sk2.z = sk1.z-30;

    PVector origin = new PVector(0, 0, 0);
    p1 = new PVector();

    // Au milieu de la premiere branche
    p1 = PVector.lerp(origin, sk1, 0.5);

    // Calculating Normal
    PVector s1 = origin;
    PVector s3 = sk1;
    PVector s2 = sk2;
    PVector v1 = s2.sub(s1);
    PVector v2 = s3.sub(s1);
    n1 = v1.cross(v2);
    n1.normalize();
    n1.mult(widthOfLeaves);
    n1.add(sk1);
    n1.z-=10;

    PVector s1b = origin;
    PVector s3b = sk2;
    PVector s2b = sk1;

    PVector v1b = s2b.sub(s1b);
    PVector v2b = s3b.sub(s1b);
    n2 = v1b.cross(v2b);
    n2.normalize();
    n2.mult(widthOfLeaves);
    n2.add(sk1);
    n2.z-=10;


    // COLORS
    c[0] = color(252, 118, 28); // set the top color (a bit lighter)
    c[1] = color(180, 233, 234); // set the top color (a bit lighter)
    c[2] = color(137, 188, 51); // set the top color (a bit lighter)
    c[3] = color(241, 195, 245); // set the top color (a bit lighter)
    c[4] = color(183, 34, 26); // set the top color (a bit lighter)


    //Echelle de la feuille
    theScale = random(sizeOfLeaves);

    // Sometimes, a leaf can be another color
    float tirage = random(100);
    if (tirage<25)
    {
      chosenColor1 = int(random(c.length));
    } else if (tirage>=25 && tirage <40)
    {
      chosenColor2 = int(random(c.length));
    }
  }
  void show() {
    pg.pushMatrix();
    pg.translate(pos.x, pos.y, pos.z);
    pg.rotateY(rotateTheLeaf);
    pg.scale(tempScale);

    pg.noStroke();
    pg.strokeWeight(1);
    //TIGE
    pg.line(0, 0, 0, p1.x, p1.y, p1.z);

    pg.beginShape(TRIANGLE_FAN);

    pg.fill(c[chosenColor1]);
    pg.vertex(sk1.x, sk1.y, sk1.z);
    pg.fill(c[chosenColor2]);
    pg.vertex(n1.x, n1.y, n1.z);
    pg.fill(c[chosenColor1]);
    pg.vertex(p1.x, p1.y, p1.z);
    pg.fill(c[chosenColor2]);
    pg.vertex(n2.x, n2.y, n2.z);
    pg.fill(c[chosenColor2]);
    pg.vertex(sk2.x, sk2.y, sk2.z);
    pg.fill(c[chosenColor2]);
    pg.vertex(n1.x, n1.y, n1.z);
    pg.endShape();
    /*DEBUG
     line(sk1.x, sk1.y, sk1.z,n1.x, n1.y, n1.z);
     line(sk1.x, sk1.y, sk1.z,n2.x, n2.y, n2.z);
     */
    pg.popMatrix();
  }

  void grow()
  {
    if (tempScale < theScale)
    {
      tempScale+=0.1;
    }
  }
}
