class Typo
{
  PFont f;
  int delayBetweenLetters = 200;
  String tempString = "";
  boolean writing = false;
  int time = 0;
  int index = 0;

  Typo()
  {
    f = createFont("monogram_extended.ttf", 100);
    textFont(f);
  }


  void display(String myText, int _x, int _y, int _size)
  {
    fill(255);
    textSize(_size);
    text(myText, _x, _y);
  }

  void displayLetterByLetter(String myText, int _x, int _y, int _size, int _start)
  {
    if (writing == false)
    {
      time = _start;
      writing = true;
    }

    if (millis()-time>delayBetweenLetters)
    {

      if (index<myText.length())
      {
        tempString += myText.charAt(index);
        time = millis();
        index++;
      }
    }
    fill(255);

    textSize(_size);
    text(tempString, _x, _y);
  }
}
