// Coding Rainbow
// Daniel Shiffman
// http://patreon.com/codingtrain
// Code for: https://youtu.be/JcopTKXt8L8

//Paramètres à gérer

//BRANCHES
// Couleur
//Nombre
//epaisseur

//FEUILLES
//Couleurs
//taille
//frequence des feuilles
//largeur

//FLEURS
//Couleurs
//taille
//frequence des feuilles
//largeur

import ch.bildspur.postfx.builder.*;
import ch.bildspur.postfx.pass.*;
import ch.bildspur.postfx.*;
import java.util.Calendar;

import processing.serial.*;

//////////////////////////////////
boolean withSerial = false;

Serial myPort;
int[] dataIn = new int[2];         // a list to hold data from the serial ports
int lf = 10;
float[] vals = new float[2];


Tree tree;

float min_dist = 10;
float max_dist = 300;


PostFX fx;
//Noise RGB filter
float xoff = 0.0;

PGraphics pg;

boolean export1 = false;
boolean export2 = false;

float Zoffset = 0;
float Yoffset = 0;
float rotationY = 0;
int margin = 100;
float maxY, minY = 0 ;
PVector vectorMaxY, vectorMinY;
String timestamp;

ArrayList <Particle> pickles = new ArrayList <Particle>();

int countGuitar= 0;
int countBass = 0;

int decreaseStepGuitar = 30;
int decreaseStepBass = 50;

Typo myText;

void setup() {
  size(1280, 720, P3D);
  //cam = new PeasyCam(this, 500);
  tree = new Tree();
  fx = new PostFX(this);  
  pg = createGraphics(width, height, P3D);
  vectorMaxY = new PVector(0, 0, 0);
  vectorMinY = new PVector(0, 0, 0);

  if (withSerial)
  {
    // print a list of the serial ports:
    printArray(Serial.list());
    // On my machine, the first and third ports in the list
    // were the serial ports that my microcontrollers were
    // attached to.
    // Open whatever ports ares the ones you're using.
    // open the ports:
    myPort = new Serial(this, "/dev/ttyUSB0", 115200);
    myPort.bufferUntil(lf);
  }
  
  myText = new Typo();
}

void draw() {
  

  pg.beginDraw();
  if (export1 == false && export2 == false)
  {
    pg.background(0);
  } else {
    pg.clear();
    println ("minY: "+minY + " maxY: "+ maxY );
  }

  for (int i=0; i<pickles.size(); i++) {
    pickles.get(i).update();
    if (pickles.get(i).getAlpha()<=0)
    {
      pickles.remove(i);
    }
  }


  pg.pushMatrix();
  pg.translate(width/2, height/2+Yoffset+margin/2, Zoffset);
  if (export1 == false && export2 == false)
  {
    pg.rotateY(rotationY);
  } else if (export1 == true)
  {
    pg.rotateY(0);
  } else if (export2 == true) {
    pg.rotateY(HALF_PI);
  }
  /*
  pg.camera(0, 0, 600.0+Zoffset, // eyeX, eyeY, eyeZ
   0.0, 0.0, 0.0, // centerX, centerY, centerZ
   0.0, 1.0, 0.0); // upX, upY, upZ
   */
  pg.pointLight(255, 255, 255, width/2, 50, 600);  
  pg.pointLight(255, 120, 120, width/2, 50, -600);  
  pg.pointLight(50, 255, 100, width, 100, 0);  

  pg.ambientLight(0, 120, 255, 0, 50, 0);  
  pg.lightSpecular(125, 50, 50);
  pg.specular(255, 255, 255);
  tree.show();  
  if (!withSerial)tree.grow();
  if (countGuitar>0)
  {
    tree.grow();
    countGuitar -= decreaseStepGuitar;
  }
  /*
  if (countBass>0)
  {
    tree.growLeafs();
    countBass -= decreaseStepBass;
  }
  */

  pg.popMatrix();
  pg.endDraw();

  image(pg, 0, 0);
  //filter(POSTERIZE, 8);
  // add bloom filter

  xoff = xoff + .1;  
  float n = noise(xoff) * 100;
    
  ///////////////////////////////////////////
  //TEXT  
  myText.display("Planete D3E6 - T1O5", 30, height-100, 50);
  myText.display("Stage 1", 30, height-50, 50);
  myText.displayLetterByLetter("Tree Generator", 30, height-10, 30, millis());
  ///////////////////////////////////////////
  fx.render()
    .pixelate(540)
    .rgbSplit(n)
    .compose();

  //println ("minY: "+minY + " maxY: "+ maxY );
  if (maxY - minY > height-margin)
  {
    //println("Zoffset "+Zoffset+ " Yoffset "+Yoffset);
    Zoffset-=10;
    Yoffset = int( height-maxY);
  }

  if (export2 == true)
  {
    cutAndExportImage(pg, "data/xport/tree"+timestamp+"_2.png");
    //pg.save("test2.png");
    export2 = false;
    Zoffset = Yoffset = maxY = minY = 0;
    tree = new Tree();
  } else
    if (export1 == true)
    {
      timestamp = timestamp();
      cutAndExportImage(pg, "data/xport/tree"+timestamp+"_1.png");
      export1 = false;
      export2 = true;

      //tree = new Tree();
    } 

  if (export1 == false && export2 == false)  minY = maxY = 0;
  rotationY+=0.01;

  println (tree.leaves.size());
  
  surface.setTitle(int(frameRate) + " fps");
}

void cutAndExportImage(PGraphics _pgToCut, String _name)
{
  PImage a;
  int[][] aPixels;
  int[][] values;

  int maxY = 0;
  int maxX = 0;
  int minY = height;
  int minX = width;
  int imageWidth;
  int imageHeight;

  PGraphics exportedImage;

  // Load the image into a new array
  // Extract the values and store in an array
  a = _pgToCut;
  aPixels = new int[a.width][a.height];
  values = new int[a.width][a.height];
  noFill();
  a.loadPixels();
  for (int i = 0; i < a.height; i++) {
    for (int j = 0; j < a.width; j++) {
      aPixels[j][i] = a.pixels[i*a.width + j];
      values[j][i] = int(alpha(aPixels[j][i]));
      if (values[j][i]>0 && i>maxY)maxY = i;
      if (values[j][i]>0 && i<minY)minY = i;
      if (values[j][i]>0 && j>maxX)maxX = j;
      if (values[j][i]>0 && j<minX)minX = j;
    }
  }

  imageWidth = maxX-minX;
  imageHeight =  maxY-minY;
  //println(minY, maxY, minX, maxX, imageWidth, imageHeight);

  PImage cuttedImage = a.get(minX, minY, imageWidth, imageHeight);

  exportedImage = createGraphics(600, 600);

  exportedImage.beginDraw();
  if (imageWidth < imageHeight)
  {
    float ratio = 600.0/float(imageHeight);
    float newWidth = imageWidth*ratio;
    exportedImage.image(cuttedImage, (600-newWidth)/2, 0, newWidth, 600);
  } else
  {
    float ratio = 600.0/float(imageWidth);
    float newHeight = imageHeight*ratio;
    exportedImage.image(cuttedImage, 0, (600-newHeight)/2, 600, newHeight);
  }
  exportedImage.endDraw();
  //temp.image(a,0,0);
  exportedImage.save(_name);
}

// timestamp
String timestamp() {
  Calendar now = Calendar.getInstance();
  return String.format("%1$ty%1$tm%1$td_%1$tH%1$tM%1$tS", now);
}



void keyPressed()
{
  if (key == 'e') export1 = true;
  else if (key == 'g') {
    countGuitar+=int(random(10, 127));
  } else if (key == 'b')
  {
    //countBass+=int(random(10,127));
    int bassRandom = int(random(0, 127));
    if (bassRandom>64)
    {
      tree.growFlowers();
    } else {
      tree.growLeafs();
    }
  }
}

void serialEvent(Serial port) {
  if (withSerial)
  {
    String str=port.readString();

    if (str==null)
      return;

    vals = float(splitTokens(str, " - "));

    //GUITAR
    if (vals[1]>0.0)
    {
      vals[1] = map(vals[1], 175, 350, 10, 255);
      vals[1] = constrain(vals[1], 10, 255);
      countGuitar += vals[1];
    }

    //BASS
    if (vals[0]>0.0)
    {
      vals[0] = map(vals[0], 175, 350, 10, 255);
      vals[0] = constrain(vals[0], 0, 255);
      //countBass += vals[0];
      if (vals[0]>127)
      {
        tree.growFlowers();
      } else {
        tree.growLeafs();
      }
    }

    //BOUM
    for (int i=0; i<500; i++) {
      pickles.add(new Particle ());
    }


    //quat.set(vals[3], vals[0], vals[1], vals[2]);
    printArray(vals);
    //Bass
    //
  }
}
