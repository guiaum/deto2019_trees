import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import ch.bildspur.postfx.builder.*; 
import ch.bildspur.postfx.pass.*; 
import ch.bildspur.postfx.*; 
import java.util.Calendar; 
import processing.serial.*; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class PlantGrow extends PApplet {

// Coding Rainbow
// Daniel Shiffman
// http://patreon.com/codingtrain
// Code for: https://youtu.be/JcopTKXt8L8

//Paramètres à gérer

//BRANCHES
// Couleur
//Nombre
//epaisseur

//FEUILLES
//Couleurs
//taille
//frequence des feuilles
//largeur

//FLEURS
//Couleurs
//taille
//frequence des feuilles
//largeur








//////////////////////////////////
boolean withSerial = false;

Serial myPort;
int[] dataIn = new int[2];         // a list to hold data from the serial ports
int lf = 10;
float[] vals = new float[2];


Tree tree;

float min_dist = 10;
float max_dist = 300;


PostFX fx;
//Noise RGB filter
float xoff = 0.0f;

PGraphics pg;

boolean export1 = false;
boolean export2 = false;

float Zoffset = 0;
float Yoffset = 0;
float rotationY = 0;
int margin = 100;
float maxY, minY = 0 ;
PVector vectorMaxY, vectorMinY;
String timestamp;

ArrayList <Particle> pickles = new ArrayList <Particle>();

int countGuitar= 0;
int countBass = 0;

int decreaseStepGuitar = 30;
int decreaseStepBass = 50;

Typo myText;

public void setup() {
  
  //cam = new PeasyCam(this, 500);
  tree = new Tree();
  fx = new PostFX(this);  
  pg = createGraphics(width, height, P3D);
  vectorMaxY = new PVector(0, 0, 0);
  vectorMinY = new PVector(0, 0, 0);

  if (withSerial)
  {
    // print a list of the serial ports:
    printArray(Serial.list());
    // On my machine, the first and third ports in the list
    // were the serial ports that my microcontrollers were
    // attached to.
    // Open whatever ports ares the ones you're using.
    // open the ports:
    myPort = new Serial(this, "/dev/ttyUSB0", 115200);
    myPort.bufferUntil(lf);
  }
  
  myText = new Typo();
}

public void draw() {
  

  pg.beginDraw();
  if (export1 == false && export2 == false)
  {
    pg.background(0);
  } else {
    pg.clear();
    println ("minY: "+minY + " maxY: "+ maxY );
  }

  for (int i=0; i<pickles.size(); i++) {
    pickles.get(i).update();
    if (pickles.get(i).getAlpha()<=0)
    {
      pickles.remove(i);
    }
  }


  pg.pushMatrix();
  pg.translate(width/2, height/2+Yoffset+margin/2, Zoffset);
  if (export1 == false && export2 == false)
  {
    pg.rotateY(rotationY);
  } else if (export1 == true)
  {
    pg.rotateY(0);
  } else if (export2 == true) {
    pg.rotateY(HALF_PI);
  }
  /*
  pg.camera(0, 0, 600.0+Zoffset, // eyeX, eyeY, eyeZ
   0.0, 0.0, 0.0, // centerX, centerY, centerZ
   0.0, 1.0, 0.0); // upX, upY, upZ
   */
  pg.pointLight(255, 255, 255, width/2, 50, 600);  
  pg.pointLight(255, 120, 120, width/2, 50, -600);  
  pg.pointLight(50, 255, 100, width, 100, 0);  

  pg.ambientLight(0, 120, 255, 0, 50, 0);  
  pg.lightSpecular(125, 50, 50);
  pg.specular(255, 255, 255);
  tree.show();  
  if (!withSerial)tree.grow();
  if (countGuitar>0)
  {
    tree.grow();
    countGuitar -= decreaseStepGuitar;
  }
  /*
  if (countBass>0)
  {
    tree.growLeafs();
    countBass -= decreaseStepBass;
  }
  */

  pg.popMatrix();
  pg.endDraw();

  image(pg, 0, 0);
  //filter(POSTERIZE, 8);
  // add bloom filter

  xoff = xoff + .1f;  
  float n = noise(xoff) * 100;
    
  ///////////////////////////////////////////
  //TEXT  
  myText.display("Planete D3E6 - T1O5", 30, height-100, 50);
  myText.display("Stage 1", 30, height-50, 50);
  myText.displayLetterByLetter("Tree Generator", 30, height-10, 30, millis());
  ///////////////////////////////////////////
  fx.render()
    .pixelate(540)
    .rgbSplit(n)
    .compose();

  //println ("minY: "+minY + " maxY: "+ maxY );
  if (maxY - minY > height-margin)
  {
    //println("Zoffset "+Zoffset+ " Yoffset "+Yoffset);
    Zoffset-=10;
    Yoffset = PApplet.parseInt( height-maxY);
  }

  if (export2 == true)
  {
    cutAndExportImage(pg, "data/xport/tree"+timestamp+"_2.png");
    //pg.save("test2.png");
    export2 = false;
    Zoffset = Yoffset = maxY = minY = 0;
    tree = new Tree();
  } else
    if (export1 == true)
    {
      timestamp = timestamp();
      cutAndExportImage(pg, "data/xport/tree"+timestamp+"_1.png");
      export1 = false;
      export2 = true;

      //tree = new Tree();
    } 

  if (export1 == false && export2 == false)  minY = maxY = 0;
  rotationY+=0.01f;

  println (tree.leaves.size());
  
  surface.setTitle(PApplet.parseInt(frameRate) + " fps");
}

public void cutAndExportImage(PGraphics _pgToCut, String _name)
{
  PImage a;
  int[][] aPixels;
  int[][] values;

  int maxY = 0;
  int maxX = 0;
  int minY = height;
  int minX = width;
  int imageWidth;
  int imageHeight;

  PGraphics exportedImage;

  // Load the image into a new array
  // Extract the values and store in an array
  a = _pgToCut;
  aPixels = new int[a.width][a.height];
  values = new int[a.width][a.height];
  noFill();
  a.loadPixels();
  for (int i = 0; i < a.height; i++) {
    for (int j = 0; j < a.width; j++) {
      aPixels[j][i] = a.pixels[i*a.width + j];
      values[j][i] = PApplet.parseInt(alpha(aPixels[j][i]));
      if (values[j][i]>0 && i>maxY)maxY = i;
      if (values[j][i]>0 && i<minY)minY = i;
      if (values[j][i]>0 && j>maxX)maxX = j;
      if (values[j][i]>0 && j<minX)minX = j;
    }
  }

  imageWidth = maxX-minX;
  imageHeight =  maxY-minY;
  //println(minY, maxY, minX, maxX, imageWidth, imageHeight);

  PImage cuttedImage = a.get(minX, minY, imageWidth, imageHeight);

  exportedImage = createGraphics(600, 600);

  exportedImage.beginDraw();
  if (imageWidth < imageHeight)
  {
    float ratio = 600.0f/PApplet.parseFloat(imageHeight);
    float newWidth = imageWidth*ratio;
    exportedImage.image(cuttedImage, (600-newWidth)/2, 0, newWidth, 600);
  } else
  {
    float ratio = 600.0f/PApplet.parseFloat(imageWidth);
    float newHeight = imageHeight*ratio;
    exportedImage.image(cuttedImage, 0, (600-newHeight)/2, 600, newHeight);
  }
  exportedImage.endDraw();
  //temp.image(a,0,0);
  exportedImage.save(_name);
}

// timestamp
public String timestamp() {
  Calendar now = Calendar.getInstance();
  return String.format("%1$ty%1$tm%1$td_%1$tH%1$tM%1$tS", now);
}



public void keyPressed()
{
  if (key == 'e') export1 = true;
  else if (key == 'g') {
    countGuitar+=PApplet.parseInt(random(10, 127));
  } else if (key == 'b')
  {
    //countBass+=int(random(10,127));
    int bassRandom = PApplet.parseInt(random(0, 127));
    if (bassRandom>64)
    {
      tree.growFlowers();
    } else {
      tree.growLeafs();
    }
  }
}

public void serialEvent(Serial port) {
  if (withSerial)
  {
    String str=port.readString();

    if (str==null)
      return;

    vals = PApplet.parseFloat(splitTokens(str, " - "));

    //GUITAR
    if (vals[1]>0.0f)
    {
      vals[1] = map(vals[1], 175, 350, 10, 255);
      vals[1] = constrain(vals[1], 10, 255);
      countGuitar += vals[1];
    }

    //BASS
    if (vals[0]>0.0f)
    {
      vals[0] = map(vals[0], 175, 350, 10, 255);
      vals[0] = constrain(vals[0], 0, 255);
      //countBass += vals[0];
      if (vals[0]>127)
      {
        tree.growFlowers();
      } else {
        tree.growLeafs();
      }
    }

    //BOUM
    for (int i=0; i<500; i++) {
      pickles.add(new Particle ());
    }


    //quat.set(vals[3], vals[0], vals[1], vals[2]);
    printArray(vals);
    //Bass
    //
  }
}
// Coding Rainbow
// Daniel Shiffman
// http://patreon.com/codingtrain
// Code for: https://youtu.be/JcopTKXt8L8

class Branch {
  Branch parent;
  PVector pos;
  PVector dir;
  int count = 0;
  PVector saveDir;
  float len = 5;
  
  Branch(PVector v, PVector d) {
    parent = null;
    pos = v.copy();
    dir = d.copy();
    saveDir = dir.copy();
  }

  Branch(Branch p) {
    parent = p;
    pos = parent.next();
    dir = parent.dir.copy();
    saveDir = dir.copy();
  }

  public void reset() {
    count = 0;
    dir = saveDir.copy();
  }

  public PVector next() {
    PVector v = PVector.mult(dir, len);
    PVector next = PVector.add(pos, v);
    return next;
  }
}
// Coding Rainbow
// Daniel Shiffman
// http://patreon.com/codingtrain
// Code for: https://youtu.be/JcopTKXt8L8



class Flower {
  // Position on the branch, when created
  PVector pos;
  
  /**
  *Flower petal
  *
  *                   sk2
  *                    |
  *                    |
  *                    |
  *                    |
  *                    |
  *                    |
  *                    sk1--------n1
  *                    |
  *                    |
  *                    |
  *                    |
  *                  origin(=pos)
  */
  PVector sk1;
  PVector sk2;
  PVector p1;
  PVector n1;
  PVector n2;
  
  //ArrayList to store petals
  ArrayList <PVector[]>  petals = new ArrayList <PVector[]>();
  int nbrOfPetals =  PApplet.parseInt (random(10, 50));
  
  // Stored color of each point
  int[] c = new int[5];
    int chosenColor1;
  int chosenColor2;

  
  // Scale of leaf
  float theScale;
  float tempScale = 0;
    
  //Alpha
  float alpha = 255;
  
  Flower(PVector _pos, int _chosenColor1, int _chosenColor2) {
    pos = _pos;
    chosenColor1 = _chosenColor1;
    chosenColor2 = _chosenColor2;
    
    //Var width of petal
    PVector offset = PVector.mult(PVector.random3D(), random(10));
    
    for (int i=0; i<nbrOfPetals; i++)
    {
     PVector[] myArrayOfPoints = new PVector[3];
     //Point1
     PVector storedSk1 =  PVector.random3D();
     storedSk1.mult(random(10));
     myArrayOfPoints[0] = storedSk1;
     
     //Point2
     PVector storedSk2 = new PVector();
     PVector.mult(storedSk1,2,storedSk2);
     myArrayOfPoints[1] = storedSk2;
     
     //Point3
     PVector storedN1 = new PVector();
     PVector.add(storedSk1,offset,storedN1);
     myArrayOfPoints[2] = storedN1;     
     
     petals.add(myArrayOfPoints);
    }

        // COLORS
    c[0] = color(255, 255, 255); // set the top color (a bit lighter)
    c[1] = color(255, 160, 222); // set the top color (a bit lighter)
    c[2] = color(62, 33, 255); // set the top color (a bit lighter)
    c[3] = color(160, 27, 23); // set the top color (a bit lighter)
    c[4] = color(252, 252, 64); // set the top color (a bit lighter)
   
     float tirage = random(100);
    if (tirage<25)
    {
      chosenColor1 = PApplet.parseInt(random(c.length));
    } else if (tirage>=25 && tirage <40)
    {
      chosenColor2 = PApplet.parseInt(random(c.length));
    } 

    
   //Echelle de la feuille
   theScale = random(2,5);
 
        
  }

  public void show() {
    pg.pushMatrix();
    pg.translate(pos.x, pos.y, pos.z);
    pg.scale(tempScale);
    
    pg.noFill();
    pg.strokeWeight(1);
    for (int i=0; i<nbrOfPetals; i++)
    {
    PVector[] myArrayOfPoints = petals.get(i);
    pg.beginShape(TRIANGLE);
    
    pg.stroke(c[0], alpha);
    pg.vertex(0, 0, 0);
    pg.stroke(c[chosenColor1]);
    pg.vertex(myArrayOfPoints[1].x, myArrayOfPoints[1].y, myArrayOfPoints[1].z);
    pg.stroke(c[chosenColor2]);
    pg.vertex(myArrayOfPoints[2].x, myArrayOfPoints[2].y, myArrayOfPoints[2].z);
    pg.endShape();
    }
    /*DEBUG
    line(sk1.x, sk1.y, sk1.z,n1.x, n1.y, n1.z);
    line(sk1.x, sk1.y, sk1.z,n2.x, n2.y, n2.z);
    */
    pg.popMatrix();
  
  }
  
  public void grow()
  {
    if (tempScale < theScale)
    {
      tempScale+=0.02f;
    }
  }
}
// Coding Rainbow
// Daniel Shiffman
// http://patreon.com/codingtrain
// Code for: https://youtu.be/JcopTKXt8L8


class Leaf {
  PVector pos;
  boolean reached = false;

  Leaf() {
    
    pos = PVector.random3D();
    pos.mult(random(width/PApplet.parseInt(random(1,4))));
    pos.y -= height/PApplet.parseInt(random(4,8));
    
 
  }

  public void reached() {
    reached = true;
  }

  public void show() {
    //if (reached)
    //{
    pg.fill(255,0,0);
    pg.noStroke();
    pg.pushMatrix();
    pg.translate(pos.x, pos.y, pos.z);
    //sphere(4);
    pg.ellipse(0,0, 10, 10);
    pg.popMatrix();
  //}
  }
}
// Coding Rainbow
// Daniel Shiffman
// http://patreon.com/codingtrain
// Code for: https://youtu.be/JcopTKXt8L8



class Leaf2 {
  // Position on the branch, when created
  PVector pos;

  /**
   *3D Leaf
   *
   *                   sk2
   *                    |
   *                    |
   *                    |
   *                    |
   *                    |
   *                    |
   *          n2-------sk1--------n1
   *                    |
   *                    |
   *                    |
   *                    |
   *                    p1  
   *                    |
   *                    |
   *                    |
   *                  origin(=pos)
   */
  PVector sk1;
  PVector sk2;
  PVector p1;
  PVector n1;
  PVector n2;

  // Stored color of each point
  int[] c = new int[5];
  int chosenColor1;
  int chosenColor2;

  // Scale of leaf
  float theScale;
  float tempScale = 0;

  //Alpha
  float alpha = 255;

  //Size of leaves
  float sizeOfLeaves;
  float widthOfLeaves;

  //Rotate the leaf ?
  float rotateTheLeaf = random(PI);

  Leaf2(PVector _pos, int _chosenColor1, int _chosenColor2, float _sizeOfLeaves, float _widthOfLeaves) {
    pos = _pos;
    chosenColor1 = _chosenColor1;
    chosenColor2 = _chosenColor2;
    sizeOfLeaves = _sizeOfLeaves;
    widthOfLeaves = _widthOfLeaves;
    //Points
    sk1 =  PVector.random3D();
    //VAR (50) -> size of leaf
    sk1.mult(random(50));
    sk2 = new PVector();
    PVector.mult(sk1, 2, sk2);
    //VAR (30) -> Courbure de la feuille
    sk2.z = sk1.z-30;

    PVector origin = new PVector(0, 0, 0);
    p1 = new PVector();

    // Au milieu de la premiere branche
    p1 = PVector.lerp(origin, sk1, 0.5f);

    // Calculating Normal
    PVector s1 = origin;
    PVector s3 = sk1;
    PVector s2 = sk2;
    PVector v1 = s2.sub(s1);
    PVector v2 = s3.sub(s1);
    n1 = v1.cross(v2);
    n1.normalize();
    n1.mult(widthOfLeaves);
    n1.add(sk1);
    n1.z-=10;

    PVector s1b = origin;
    PVector s3b = sk2;
    PVector s2b = sk1;

    PVector v1b = s2b.sub(s1b);
    PVector v2b = s3b.sub(s1b);
    n2 = v1b.cross(v2b);
    n2.normalize();
    n2.mult(widthOfLeaves);
    n2.add(sk1);
    n2.z-=10;


    // COLORS
    c[0] = color(252, 118, 28); // set the top color (a bit lighter)
    c[1] = color(180, 233, 234); // set the top color (a bit lighter)
    c[2] = color(137, 188, 51); // set the top color (a bit lighter)
    c[3] = color(241, 195, 245); // set the top color (a bit lighter)
    c[4] = color(183, 34, 26); // set the top color (a bit lighter)


    //Echelle de la feuille
    theScale = random(sizeOfLeaves);

    // Sometimes, a leaf can be another color
    float tirage = random(100);
    if (tirage<25)
    {
      chosenColor1 = PApplet.parseInt(random(c.length));
    } else if (tirage>=25 && tirage <40)
    {
      chosenColor2 = PApplet.parseInt(random(c.length));
    }
  }
  public void show() {
    pg.pushMatrix();
    pg.translate(pos.x, pos.y, pos.z);
    pg.rotateY(rotateTheLeaf);
    pg.scale(tempScale);

    pg.noStroke();
    pg.strokeWeight(1);
    //TIGE
    pg.line(0, 0, 0, p1.x, p1.y, p1.z);

    pg.beginShape(TRIANGLE_FAN);

    pg.fill(c[chosenColor1]);
    pg.vertex(sk1.x, sk1.y, sk1.z);
    pg.fill(c[chosenColor2]);
    pg.vertex(n1.x, n1.y, n1.z);
    pg.fill(c[chosenColor1]);
    pg.vertex(p1.x, p1.y, p1.z);
    pg.fill(c[chosenColor2]);
    pg.vertex(n2.x, n2.y, n2.z);
    pg.fill(c[chosenColor2]);
    pg.vertex(sk2.x, sk2.y, sk2.z);
    pg.fill(c[chosenColor2]);
    pg.vertex(n1.x, n1.y, n1.z);
    pg.endShape();
    /*DEBUG
     line(sk1.x, sk1.y, sk1.z,n1.x, n1.y, n1.z);
     line(sk1.x, sk1.y, sk1.z,n2.x, n2.y, n2.z);
     */
    pg.popMatrix();
  }

  public void grow()
  {
    if (tempScale < theScale)
    {
      tempScale+=0.1f;
    }
  }
}
class Particle {
 
  float x;
  float y;
  float alpha = 255;
  
  float velX; // speed or velocity
  float velY;
  
  
  Particle () {
   //x and y position to be in middle of screen
    x = width/2;
    y = height/4*3;
    
    
    velX = random (-20,20);
    velY = random (-25,25);
    
   
  }
  
  public void update () {
    
    x+=velX;
    y+=velY;
    
    pg.fill (255, alpha);
    pg.noStroke();
    pg.ellipse (x,y,3,3);
    alpha -=5;
  }
  
  public float getAlpha()
  {
   return alpha; 
  }
  
  
}
// Coding Rainbow
// Daniel Shiffman
// http://patreon.com/codingtrain
// Code for: https://youtu.be/JcopTKXt8L8




class Tree {
  ArrayList<Branch> branches = new ArrayList<Branch>();
  //Invisible, only for positionning branches
  ArrayList<Leaf> leaves = new ArrayList<Leaf>();
  //Visible Leaves
  ArrayList<Leaf2> leaves2 = new ArrayList <Leaf2>();
  //Visible flowers
  ArrayList<Flower> flowers = new ArrayList <Flower>();
  boolean leaves2Done = false;

  int nbreOfBranches = PApplet.parseInt(random(120, 250));

  //LEAFS2
  //COLORS
  int choseColorLeaf1 = PApplet.parseInt(random(5));
  int choseColorLeaf2 = PApplet.parseInt(random(5));
  float sizeOfLeaves = random(0.5f, 4);
  float widthOfLeaves = random(5, 40);

  //nbr
  int nbreOfLeavesVisible = PApplet.parseInt(random(7, 9));


  Tree() {
    //500 nbre de feuilles
    for (int i = 0; i < nbreOfBranches; i++) {
      leaves.add(new Leaf());
    }    
    //height/6 hauteur du tronc
    Branch root = new Branch(new PVector(100, height-200), new PVector(0, -1));
    branches.add(root);
    Branch current = new Branch(root);

    while (!closeEnough(current)) {
      Branch trunk = new Branch(current);
      branches.add(trunk);
      current = trunk;
    }

    println("New Tree");
    println ("Branches: " + nbreOfBranches);
    println ("sizeOfLeaves: "+sizeOfLeaves);
    println ("widthOfLeaves:" +widthOfLeaves);
    println("nbreOfLeavesVisible: "+ nbreOfLeavesVisible);
  }

  public boolean closeEnough(Branch b) {

    for (Leaf l : leaves) {
      float d = PVector.dist(b.pos, l.pos);
      if (d < max_dist) {
        return true;
      }
    }
    return false;
  }

  public void growBass(float val) {
  }

  public void growGuitar(float val) {
  }



  public void grow() {
    for (Leaf l : leaves) {
      Branch closest = null;
      PVector closestDir = null;
      float record = -1;

      for (Branch b : branches) {
        PVector dir = PVector.sub(l.pos, b.pos);
        float d = dir.mag();
        if (d < min_dist) {
          l.reached();
          closest = null;
          break;
        } else if (d > max_dist) {
        } else if (closest == null || d < record) {
          closest = b;
          closestDir = dir;
          record = d;
        }
      }
      if (closest != null) {
        closestDir.normalize();
        closest.dir.add(closestDir);
        closest.count++;
      }
    }


    for (int i = leaves.size()-1; i >= 0; i--) {
      if (leaves.get(i).reached) {
        leaves.remove(i);
      }
    }


    for (int i = branches.size()-1; i >= 0; i--) {
      Branch b = branches.get(i);
      if (b.count > 0) {
        b.dir.div(b.count);
        PVector rand = PVector.random2D();
        //tortuosité
        // 0.1 très droit, 1 très tortueux, 
        rand.setMag(random(0.1f, 1));
        b.dir.add(rand);
        b.dir.normalize();
        Branch newB = new Branch(b);
        branches.add(newB);
        //LEAVES
        if (random(10)>nbreOfLeavesVisible)leaves2.add(new Leaf2(newB.pos, choseColorLeaf1, choseColorLeaf2, sizeOfLeaves, widthOfLeaves ));
        //FLOWER
        if (random(100)>95)flowers.add(new Flower(newB.pos, choseColorLeaf1, choseColorLeaf2));
        //println(screenY(newB.pos.x, newB.pos.y, newB.pos.z));
        //if(screenY(newB.pos.x, newB.pos.y, newB.pos.z)<0)Zoffset++;
        b.reset();
      }
    }
  }

  public void growLeafs()
  {
    for (Leaf2 l2 : leaves2) {
      //l2.show();
      l2.grow();
    }
  }

  public void growFlowers()
  {
    for (Flower f : flowers) {
      //f.show();
      f.grow();
    }
  }

  public void show() {
    
    for (Leaf l : leaves) {
     l.show();
     }  
     

    for (Leaf2 l2 : leaves2) {
      l2.show();
      //l2.grow();
    }   

    for (Flower f : flowers) {
      f.show();
      //f.grow();
    }  



    //for (Branch b : branches) {
    for (int i = 0; i < branches.size(); i++) {
      Branch b = branches.get(i);
      if (b.parent != null) {
        float sw = map(i, 0, branches.size(), 6, 0);
        pg.strokeWeight(sw);
        pg.stroke(200);
        pg.line(b.pos.x, b.pos.y, b.pos.z, b.parent.pos.x, b.parent.pos.y, b.parent.pos.z);

        //
        if (pg.screenY(b.pos.x, b.pos.y, b.pos.z ) >maxY)
        {
          maxY = pg.screenY(b.pos.x, b.pos.y, b.pos.z);
        } else if (pg.screenY(b.pos.x, b.pos.y, b.pos.z ) <minY)
        {
          minY = pg.screenY(b.pos.x, b.pos.y, b.pos.z);
        }
      }
    }
  }
}
class Typo
{
  PFont f;
  int delayBetweenLetters = 200;
  String tempString = "";
  boolean writing = false;
  int time = 0;
  int index = 0;

  Typo()
  {
    f = createFont("monogram_extended.ttf", 100);
    textFont(f);
  }


  public void display(String myText, int _x, int _y, int _size)
  {
    fill(255);
    textSize(_size);
    text(myText, _x, _y);
  }

  public void displayLetterByLetter(String myText, int _x, int _y, int _size, int _start)
  {
    if (writing == false)
    {
      time = _start;
      writing = true;
    }

    if (millis()-time>delayBetweenLetters)
    {

      if (index<myText.length())
      {
        tempString += myText.charAt(index);
        time = millis();
        index++;
      }
    }
    fill(255);

    textSize(_size);
    text(tempString, _x, _y);
  }
}
  public void settings() {  size(1280, 720, P3D); }
  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "PlantGrow" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}
