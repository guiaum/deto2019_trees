// Coding Rainbow
// Daniel Shiffman
// http://patreon.com/codingtrain
// Code for: https://youtu.be/JcopTKXt8L8




class Tree {
  ArrayList<Branch> branches = new ArrayList<Branch>();
  //Invisible, only for positionning branches
  ArrayList<Leaf> leaves = new ArrayList<Leaf>();
  //Visible Leaves
  ArrayList<Leaf2> leaves2 = new ArrayList <Leaf2>();
  //Visible flowers
  ArrayList<Flower> flowers = new ArrayList <Flower>();
  boolean leaves2Done = false;

  int nbreOfBranches = int(random(120, 250));

  //LEAFS2
  //COLORS
  int choseColorLeaf1 = int(random(5));
  int choseColorLeaf2 = int(random(5));
  float sizeOfLeaves = random(0.5, 4);
  float widthOfLeaves = random(5, 40);

  //nbr
  int nbreOfLeavesVisible = int(random(7, 9));


  Tree() {
    //500 nbre de feuilles
    for (int i = 0; i < nbreOfBranches; i++) {
      leaves.add(new Leaf());
    }    
    //height/6 hauteur du tronc
    Branch root = new Branch(new PVector(100, height-200), new PVector(0, -1));
    branches.add(root);
    Branch current = new Branch(root);

    while (!closeEnough(current)) {
      Branch trunk = new Branch(current);
      branches.add(trunk);
      current = trunk;
    }

    println("New Tree");
    println ("Branches: " + nbreOfBranches);
    println ("sizeOfLeaves: "+sizeOfLeaves);
    println ("widthOfLeaves:" +widthOfLeaves);
    println("nbreOfLeavesVisible: "+ nbreOfLeavesVisible);
  }

  boolean closeEnough(Branch b) {

    for (Leaf l : leaves) {
      float d = PVector.dist(b.pos, l.pos);
      if (d < max_dist) {
        return true;
      }
    }
    return false;
  }

  void growBass(float val) {
  }

  void growGuitar(float val) {
  }



  void grow() {
    for (Leaf l : leaves) {
      Branch closest = null;
      PVector closestDir = null;
      float record = -1;

      for (Branch b : branches) {
        PVector dir = PVector.sub(l.pos, b.pos);
        float d = dir.mag();
        if (d < min_dist) {
          l.reached();
          closest = null;
          break;
        } else if (d > max_dist) {
        } else if (closest == null || d < record) {
          closest = b;
          closestDir = dir;
          record = d;
        }
      }
      if (closest != null) {
        closestDir.normalize();
        closest.dir.add(closestDir);
        closest.count++;
      }
    }


    for (int i = leaves.size()-1; i >= 0; i--) {
      if (leaves.get(i).reached) {
        leaves.remove(i);
      }
    }


    for (int i = branches.size()-1; i >= 0; i--) {
      Branch b = branches.get(i);
      if (b.count > 0) {
        b.dir.div(b.count);
        PVector rand = PVector.random2D();
        //tortuosité
        // 0.1 très droit, 1 très tortueux, 
        rand.setMag(random(0.1, 1));
        b.dir.add(rand);
        b.dir.normalize();
        Branch newB = new Branch(b);
        branches.add(newB);
        //LEAVES
        if (random(10)>nbreOfLeavesVisible)leaves2.add(new Leaf2(newB.pos, choseColorLeaf1, choseColorLeaf2, sizeOfLeaves, widthOfLeaves ));
        //FLOWER
        if (random(100)>95)flowers.add(new Flower(newB.pos, choseColorLeaf1, choseColorLeaf2));
        //println(screenY(newB.pos.x, newB.pos.y, newB.pos.z));
        //if(screenY(newB.pos.x, newB.pos.y, newB.pos.z)<0)Zoffset++;
        b.reset();
      }
    }
  }

  void growLeafs()
  {
    for (Leaf2 l2 : leaves2) {
      //l2.show();
      l2.grow();
    }
  }

  void growFlowers()
  {
    for (Flower f : flowers) {
      //f.show();
      f.grow();
    }
  }

  void show() {
    
    for (Leaf l : leaves) {
     l.show();
     }  
     

    for (Leaf2 l2 : leaves2) {
      l2.show();
      //l2.grow();
    }   

    for (Flower f : flowers) {
      f.show();
      //f.grow();
    }  



    //for (Branch b : branches) {
    for (int i = 0; i < branches.size(); i++) {
      Branch b = branches.get(i);
      if (b.parent != null) {
        float sw = map(i, 0, branches.size(), 6, 0);
        pg.strokeWeight(sw);
        pg.stroke(200);
        pg.line(b.pos.x, b.pos.y, b.pos.z, b.parent.pos.x, b.parent.pos.y, b.parent.pos.z);

        //
        if (pg.screenY(b.pos.x, b.pos.y, b.pos.z ) >maxY)
        {
          maxY = pg.screenY(b.pos.x, b.pos.y, b.pos.z);
        } else if (pg.screenY(b.pos.x, b.pos.y, b.pos.z ) <minY)
        {
          minY = pg.screenY(b.pos.x, b.pos.y, b.pos.z);
        }
      }
    }
  }
}
